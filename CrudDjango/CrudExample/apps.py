from django.apps import AppConfig


class CrudexampleConfig(AppConfig):
    name = 'CrudExample'
