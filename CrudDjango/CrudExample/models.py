from django.db import models

# Create your models here.

class Account(models.Model):
    userName = models.CharField(max_length= 10)
    passwd = models.CharField(max_length= 10)
    showName = models.CharField(max_length= 30)
    email = models.CharField(max_length= 30)
    address = models.CharField(max_length=20)
    phone = models.CharField(max_length=11)

    class Meta:  
        db_table = "Account"