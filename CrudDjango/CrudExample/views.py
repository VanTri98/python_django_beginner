from django.shortcuts import render, redirect
from CrudExample.forms import AccountForm
from CrudExample.models import Account

# Create your views here.
def addAcc(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if(form.is_valid)():
            try:
                form.save()
                return redirect("/show")
            except:
                pass
    else:
        form =AccountForm()
    return render(request,'addAcc.html',{'form':form})

def showAcc(request):
    acc = Account.objects.all()
    return render(request,"index.html",{'accounts':acc})

def edit(request,id):
    account = Account.objects.get(id = id)
    return render(request,'edit.html',{'acc':account})

def update(request,id):
    account =  Account.objects.get(id = id)
    form = AccountForm(request.POST, instance = account)
    if form.is_valid():
        try:
                form.save()
                return redirect("/show")
        except:
                pass
    return render(request,'edit.html',{'acc':account})

def delete(request,id):
    account  = Account.objects.get(id = id)
    account.delete()
    return redirect("/show")