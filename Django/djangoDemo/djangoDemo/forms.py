from django import forms
from djangoDemo.models import Account

class AccForm(forms.ModelForm):
    class Meta:
        model = Account
        fields = "__all__"