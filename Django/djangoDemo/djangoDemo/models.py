from __future__ import unicode_literals
from django.db import models

class Account(models.Model):
    # CharField là 1 lớp được dùng để tạo input bên html
    # max_length để set độ dài cho chuỗi trong input

    userName = models.CharField(max_length = 30)
    password = models.CharField(max_length = 30)
    email = models.EmailField()
    fileUpload = models.FileField()
    class Meta:
        db_table = "Account"
