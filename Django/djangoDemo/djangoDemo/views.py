# HttpResponse được sử dụng để truyền thông tin (trả về response)
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.template import loader
from djangoDemo.forms import AccForm
from djangoDemo.functions import upload_file
import datetime

# Tạo yêu cầu để gửi đi
def HelloWorld(request):
    return HttpResponse("Hello World !!!")

def dateOfTime(request):
    now = datetime.datetime.now()

    # convert datetime sang kiểu string
    html = "Bây giờ là: {}".format(now)

    return HttpResponse(html)

def home(request):
    template = loader.get_template("home.html")
    show = {
        'display': 'My website'
    }
    # return render(request,'home.html') 
    return HttpResponse(template.render(show))

def index(request):
    if request.method == "POST":
        form = AccForm(request.POST,request.FILES)
        # sử dụng is_valid để xác thực dữ liệu
        if form.is_valid():
            try:
                upload_file(request.FILES['fileUpload'])
                # trường hợp dữ liệu hợp lệ
                # return redirect("/")
                return HttpResponse("Successfuly")
            except:
                pass
    else:
        form = AccForm()
    return render(request,"index.html",{'form': AccForm()})


