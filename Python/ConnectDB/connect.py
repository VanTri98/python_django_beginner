# import package
from mysql.connector import MySQLConnection

# Viết hàm kết nối
def connectDB():
    """ kết nối MySQL  bằng Module MySQL Connection """
    db_config = {
        'host': 'localhost',
        'database':'sale',
        'user': 'root',
        'password': ''
    }

    # Tạo 1 biến kết nối
    conn = None
    conn = MySQLConnection(**db_config)
    try:
        if conn.is_connected():
            return conn
    except Exception as error:
        print(error)
 
    return conn

# Thực hiện test thử
conn = connectDB()
print(conn)
