import students
st = students.Student()
choice = 0
while choice >= 0:
    if choice == 1:
        st.addStudent()
    elif choice == 2:
        st.showStudent()
    elif choice == 3:
        st.editStudent()
    elif choice == 4:
        st.delStudent()
        
    print("Chọn chức năng muốn thực hiện:")
    print("Nhập 1: Thêm sinh viên")
    print("Nhập 2: Xem danh sách sinh viên")
    print("Nhập 3: Sửa sinh viên")
    print("Nhập 4: Xóa sinh viên")
    print("Nhập 0: Thoát khỏi chương trình")
    print("Sự lựa chọn của ban là: ")
    choice = int(input())
    if choice == 0:
        break