class Student:
    # Tạo mảng chứa dữ liệu
    listStudents =[]


    # kiểm tra sinh viên
    def findStudent(self,id):
        for i in range(0, len(self.listStudents)):
            if self.listStudents[i]['id'] == id:

                # nếu tìm thấy sinh viên: nó sẽ trả về: 
                # 1, vị trí của sv đó trong mảng
                # 2, Dữ liệu của sinh viên đó
                return [i, self.listStudents[i]]
        return False

    # Hàm thêm sinh viên
    def addStudent(self):
        print("---Them sinh vien---")

        # su dung dict de luu tru sinh vien
        inforStudents = {
        'id': 0,
        'name': "",
        'age': 0
        }
        print("Nhập id: ")
        id = int(input())
        while True:
            st = self.findStudent(id)
            if st != False:
                print("Mã sinh viên đã tồn tại! Mời bạn nhập lại id: ")
                id = int(input())
            else: break
        
        inforStudents['id'] = id

        print("Nhập tên: ")
        inforStudents['name']  = str(input())
        print("Nhập tuổi: ")
        inforStudents['age']  = int(input())

        self.listStudents.append(inforStudents)

    # Show thông tin sinh viên
    def showStudent(self):
        print("***Danh sách sinh vien***")
        for i in range(0 ,len(self.listStudents)):
            print("Mã sv",(i+1),":",self.listStudents[i]['id'])
            print("Tên sv:",self.listStudents[i]['name'])
            print("Tuổi:",self.listStudents[i]['age'])
            print("")

    # Chỉnh sửa thông tin sinh viên
    def editStudent(self):
        print("Sửa thông tin sinh viên")
        print("Nhâp id sv cần sửa: ")
        id = int(input())
        std = self.findStudent(id)
        if  std == False:
            print("Sinh viên này không tồn tại",id)
        else: 
            print("Nhập tên sinh viên: ")
            name = input()
            print("Nhập tuổi: ")
            age = input()
            std[1]['name'] = name
            std[1]['age'] = age
            self.listStudents[std[0]] = std[1]

    # Xóa sinh viên
    def delStudent(self):
        print("Xóa sinh viên")
        print("Nhập id cần xóa: ")
        id = int(input())
        std = self.findStudent(id)

        if std != False:
            self.listStudents.remove(std[1])
            print("Xóa thành công")
        else:
            print("Không tìm thấy sinh viên cần xóa")





    



