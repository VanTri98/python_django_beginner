# sử dụng mysqk.connector từ MYSQLConnection
from mysql.connector import MySQLConnection, Error

# Viết hàm kết nối với Database
def connectionDB():
    """ Tạo 1 biến để lưu cấu hình kết nối """
    database_config = {
        'host': 'localhost',
        'database': 'sale',
        'user': 'root',
        'password': ''
    }

    # Tạo biến kết nối
    connect = MySQLConnection(**database_config)
    try:
        if connect.is_connected():
            return connect
    except Error as error:
        connect.close()
        print(error)

    return connect

print(connectionDB())