import process 
acc = process.Account()

print("*** Quản lý tài khoản người dùng ***")

choice = 0
while choice >=0:
    print("----------------")
    print("1. Xem danh sách tài khoản")
    print("2. Thêm tài khoản người dùng")
    print("3. Cập nhật tên tài khoản")
    print("4. Xóa tài khoản")
    print("0. Thoát")
    print("Mời bạn nhập lựa chọn:")
    choice = int(input())

    if choice == 1:
        acc.showAccount()
    if choice == 2:
        print("---Thêm tài khoản---")
        print("Nhập tên đăng nhập: ")
        userName = str(input())
        print("Nhập mật khẩu: ")
        passwd = str(input())
        print("Nhập tên hiển thị: ")
        showName = str(input())
        print("Nhập email: ")
        email = str(input())
        print("Nhập địa chỉ: ")
        address = str(input())
        print("Nhập số điện thoại: ")
        phone = str(input())
        acc.insert(userName,passwd,showName,email,address,phone)
    if choice == 3:
        print("Cập nhật tên hiển thị")
        print("Mời bạn nhập ID cần sửa: ")
        id = int(input())
        if acc.getById(id):
            print("Tên hiển thị: ")
            showName = str(input())
            acc.update(id,showName)
        else:
            print("Tài khoản không tồn tại")
    if choice == 4:
        print("Xóa tài khoản người dùng")
        print("Mời bạn nhập ID cần sửa: ")
        id = int(input())
        if acc.getById(id):
            acc.delete(id)
        else:
            print("Tài khoản không tồn tại")
    if choice == 0:
        break
