import connectDB

class Account:

    conn = None

    # Hàm khởi tạo constructor
    def __init__(self):
        self.connectDB()

    def __del__(self):
        if self.conn != None:
            self.conn.close()

    def connectDB(self):
        conn = connectDB.connectionDB()
        self.conn = conn



    def showAccount(self):
        try:
            cursor = self.conn.cursor()

            # Thực hiện truy vấn
            cursor.execute("SELECT * FROM ACCOUNT ")

            # Sử dụng biến row để lấy dữ liệu trả về từ db -- lấy 1 dòng dùng fetchone
            # row sẽ được trả về kiểu tuple
            row = cursor.fetchone()

            # Tạo 1 biến mảng để convert dữ liệu từ tuple sang array (array này ta sẽ để là mảng dict)
            arrayData = list()
            while row is not None:
                # duyệt từng cell trong 1 row
                myRow = iter(row)
                # add các data trong cell đó vào arrayData bằng next()
                arrayData.extend([
                    {
                        'id': next(myRow),
                        'userName':next(myRow),
                        'pass':next(myRow),
                        'showName':next(myRow),
                        'email':next(myRow),
                        'address':next(myRow),
                        'phone': next(myRow)
                    }
                ])
                row = cursor.fetchone()

            # lấy dữ liệu dạng dict từ arrayData
            print()
            print("---Thông tin tài khoản người dùng---")
            count = 1
            for i in arrayData:
                print("STT: "+str(count))
                print("ID: "+str(i.get("id")))
                print("Tên đăng nhập: "+i.get("userName"))
                print("Mật khẩu: "+i.get("pass"))
                print("Tên người dùng: "+i.get("showName"))
                print("Email: "+i.get("email"))
                print("Địa chỉ: "+i.get("address"))
                print("Số điện thoại: "+(i.get("phone")))
                print() 
                count = count +1

        except Exception as e:
            print(e)
    
    # Tìm kiếm theo ID
    def getById(self,id):
        try:
            conn = connectDB.connectionDB()
            cursor = conn.cursor()
            cursor.execute("SELECT * FROM account WHERE id= "+str(id))
            row = cursor.fetchone()
            if row == None:
                return False
            return True
        except Exception as e:
                print(e)

    def insert(self, userName, passwd, showName, email, address, phone):
        sql = "INSERT INTO ACCOUNT(userName,pass,showName,email,address,phone) VALUES(%s,%s,%s,%s,%s,%s)"
        args = (userName, passwd, showName, email, address, phone)

        try:
            conn = connectDB.connectionDB()
            cursor = conn.cursor()

            cursor.execute(sql,args)
            if cursor.lastrowid:
                print("Thêm mới thành công")
            else:
                print("Lỗi khi thêm mới")

            conn.commit()
            self.showAccount()

        except Exception as e:
            print(e)
        finally:
            cursor.close()

    def update(self,id,showName):
        sql = "UPDATE ACCOUNT SET showName = %s WHERE ID = %s "
        data = (showName,id)

        try:
            conn = connectDB.connectionDB()
            cursor = conn.cursor()

            cursor.execute(sql,data)
            conn.commit()

            self.showAccount()
        except Exception as e:
            print(e)
        finally:
            cursor.close()

    
    def delete(self,id):
        query = "DELETE FROM ACCOUNT WHERE id = %s"
        try:
            conn = connectDB.connectionDB()
            cursor = conn.cursor()
            cursor.execute(query, (id,))
            conn.commit()
            print("Xóa thành công")
            self.showAccount()
 
        except Exception as error:
            print(error)
 
        finally:
            cursor.close()
   


