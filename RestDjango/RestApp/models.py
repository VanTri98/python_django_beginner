from django.db import models

# Create your models here.
# Định nghĩa model Account trong file models.py

class Admin(models.Model):
    id = models.IntegerField
    adminName = models.CharField(max_length= 50)
    passwd = models.CharField(max_length= 10)
    showName = models.CharField(max_length= 50)
    auth = models.CharField(max_length= 50)


    def get_info(self):
        return 'Tên chủ tài khoản: ' +self.showName