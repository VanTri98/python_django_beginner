from django.shortcuts import render, get_object_or_404
from django.http import JsonResponse, HttpResponse

from rest_framework import status
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView

from .serializers import AdminSerializers
from .models import Admin

# Create your views here.
class ListCreateAdminView(ListCreateAPIView):
    model = Admin
    serializer_class = AdminSerializers

    def get_queryset(self):
        return Admin.objects.all()

    def create(self, request, *args, **kwargs):
        serializer = AdminSerializers(data=request.data)

        if serializer.is_valid():
            serializer.save()

            return HttpResponse('Create a new Admin successful!')
    
        return HttpResponse('Create a new Admin unsuccessful!')

class UpdateDelAdminView(RetrieveUpdateDestroyAPIView):
    model = Admin
    serializer_class = AdminSerializers

    def put(self,request,*args,**kwargs):
        admin = get_object_or_404(Admin, id= kwargs.get('id'))
        serializer = AdminSerializers(admin,data=request.data)

        if serializer.is_valid():
            serializer.save()

            return JsonResponse({
                'mess' : 'Update Admin Successfull !'
            }, status = status.HTTP_200_OK)
        
        return JsonResponse({
            'mess': 'Update Admin Unsuccessfull !'
        }, status = status.HTTP_400_BAD_REQUEST)
    
    def delete(self,request,*args,**kwargs):
        admin =get_object_or_404(Admin, id=kwargs.get('id'))
        admin.delete()
        
        return JsonResponse({
            'mess': 'Delete Admin successful!'
        }, status=status.HTTP_200_OK)
