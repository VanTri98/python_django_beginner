from django.db import models

# Create your models here.
class Employee(models.Model):
    name = models.CharField(max_length= 50)
    age = models.IntegerField(default= 0)
    address = models.CharField(max_length=100)
    description = models.TextField(blank=True)

    class Meta:
        db_table = "employee"
    
    def get_info(self):
        return 'name: '+self.name + 'age: '+self.age+ 'address: '+self.address+ 'email: '+self.email
