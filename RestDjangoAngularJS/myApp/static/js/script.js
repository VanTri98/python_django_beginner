var myApp = angular.module('myApp', []);


myApp.controller('employeeCtrl', function ($scope, $http) {
	
    $scope.openAdd = function(){
		console.log($scope.empAge)
	$scope.empAge = false;
    $scope.bindAdd = "Thêm mới"
	$scope.emp = {}
        document.getElementById("myPopup").style.display = "block";
		document.getElementById("update").style.display = "none";
		document.getElementById("insert").style.display = "block";

    }

    $scope.insert = function(emp){
        $http({
			method: 'POST',
			url: '/insert',
			data: emp
		}).then(function() {
			alert("You inserted successfully !!! ");
			document.getElementById("myPopup").style.display = "none";
		})

    }

    $scope.openEdit = function(id){
        $scope.bindAdd = "Cập nhật"
		$scope.empAge = false;
        $scope.emp = {
            id : id,
			name: $('#name_'+id).html(),
			address: $('#address_'+id).html(),
			age: $('#age_'+id).html(),
			description: $('#description_'+id).html()
		}
     
		document.getElementById("myPopup").style.display = "block";
		document.getElementById("update").style.display = "block";
		document.getElementById("insert").style.display = "none";

    }

    $scope.update = function(dataItem){
        $http(
			{
				method: 'POST',
				url: '/update/' + dataItem.id,
				data: dataItem
			},
            alert('Cập nhật thành công')
		).then(function() {
            
			document.getElementById("myPopup").style.display = "none";
		})

    }

    $scope.del = function(id){
        var check = confirm('Bạn có muốn xóa ?');
		if (check) {
			$http.delete('/delete/' + id).then(function() {
                location.reload();
				alert('Xóa thành công');
			});
		}
		document.getElementById("myPopup").style.display = "none";

    }
});

myApp.directive('myDirective', function() {
	return {
		restrict: "E",
		template: '<h2 style="text-align: center;">My Directive</h2>'
	};
});

myApp.directive('checkNumber',function(){
    return{
		restrict: 'A',
		require: 'ngModel',
		link: function($scope, element, attr, employeeCtrl){
			function validateNumber(value){
				var ch= /[a-zA-ZẠ-ỹáàạảãâấầậẩẫăắằặẳẵÁÀẠẢÃÂẤẦẬẨẪĂẮẰẶẲẴéèẹẻẽêếềệểễÉÈẸẺẼÊẾỀỆỂỄóòọỏõôốồộổỗơớờợởỡÓÒỌỎÕÔỐỒỘỔỖƠỚỜỢỞỠúùụủũưứừựửữÚÙỤỦŨƯỨỪỰỬỮíìịỉĩÍÌỊỈĨđĐýỳỵỷỹÝỲỴỶỸ!@#$%^&*()+\=\[\]{};':"_\\|,.<>\-\\/?]/;
				if(!ch.test(value)){
					$scope.empAge = true
				}else{
					$scope.empAge = false
				}
				console.log("a",$scope.empAge)
				return $scope.empAge;
			}
			employeeCtrl.$parsers.push(validateNumber)
		}
	}
})
