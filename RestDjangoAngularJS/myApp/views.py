from django.shortcuts import render, get_object_or_404
from django.http import JsonResponse, HttpResponse
from django.views.generic import TemplateView, View, DeleteView

from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import api_view

from myApp.models import Employee
from myApp.serializers import EmployeeSerializer

# Create your views here.
@api_view(['GET'])
def get_employee(request):
    if request.method == 'GET':
        employees = Employee.objects.all()
        serializer = EmployeeSerializer(employees, many =True)
        return render(request,'index.html',{'employees':employees})
    return Response(serializer.errors, status= status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
def add_employee(request):
    if request.method == 'POST':
        data = {
            'name' : request.data.get('name'),
            'address' : request.data.get('address'),
            'age' : int(request.data.get('age')),
            'description' : request.data.get('description')

        }
        print(data)
        serializer = EmployeeSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status= status.HTTP_201_CREATED)
        return Response(serializer.errors, status= status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
def update_employee(request,id):
    try:    
            data = {
                'id': id,
                'name' : request.data.get('name'),
                'address' : request.data.get('address'),
                'age' : int(request.data.get('age')),
                'description' : request.data.get('description')

            }
            employee = Employee.objects.get(id=id)
    except Employee.DoesNotExist:
            return Response(serializer.errors, status= status.HTTP_404_NOT_FOUND)
    if request.method == 'POST':
            serializer = EmployeeSerializer(employee,data=data)
            if serializer.is_valid():   
                serializer.save()  
                return Response(status=status.HTTP_204_NO_CONTENT)
            return Response(status=status.HTTP_400_BAD_REQUEST)


@api_view(['DELETE'])
def delete_employee(request,id):
        try:
             employee = Employee.objects.get(id=id)
        except Employee.DoesNotExist:
            return Response(serializer.errors, status= status.HTTP_404_NOT_FOUND)
        
        if request.method == 'DELETE':
            employee.delete()
            return Response(status=status.HTTP_204_NO_CONTENT)

    
